describe("Haagendazs", () => {
  context("Homepage", () => {
    beforeEach(() => cy.visit("/"));

    it("successfully searches for 'chocolate'", () => {
      cy.get("#searchDisplay").click();
      cy.get(".form-search").type("chocolate{enter}");

      cy.url().should("eq", `${Cypress.config("baseUrl")}/search/node?keys=chocolate`);
    });
  });

  context("Search page", () => {
    beforeEach(() => cy.visit("/search/node?keys=chocolate"));

    it("does not show the last pagination button after clicking on it", () => {
      cy.get(".pager__item--last a").as("paginationLastButton").click();

      cy.get("paginationLastButton").should("not.be.visible");
    });
  });

  context("API", () => {
    it("returns success status code when requesting to the search endpoint", () => {
      cy.request(`${Cypress.config("baseUrl")}/search/node?keys=chocolate&page=18`)
        .then(response => expect(response.status).to.equal(200));
    });
  });
});