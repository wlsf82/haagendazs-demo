const helper = require("protractor-helper");

describe("Haagendazs", () => {
  describe("Homepage", () => {
    const Homepage = require("../page-objects/Homepage");

    beforeEach(() => browser.get("/"));

    it("successfully searches for 'chocolate'", () => {
      const homepage = new Homepage();
  
      helper.click(homepage.searchIcon);
      helper.fillFieldWithTextAndPressEnter(homepage.searchInputField, "chocolate");
  
      helper.waitForUrlToBeEqualToExpectedUrl(`${browser.baseUrl}/search/node?keys=chocolate`);
    });
  });

  describe("Search page", () => {
    const Searchpage = require("../page-objects/Searchpage");

    beforeEach(() => browser.get("/search/node?keys=chocolate"));

    it("does not show the last pagination button after clicking on it", () => {
      const searchpage = new Searchpage();

      helper.scrollToElement(searchpage.paginationLastButton);
      helper.click(searchpage.paginationLastButton);

      helper.waitForElementNotToBeVisible(searchpage.paginationLastButton);
    });
  });
});