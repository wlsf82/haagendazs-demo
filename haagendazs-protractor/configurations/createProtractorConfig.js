module.exports = (providedConfig) => {
  const defaultConfig = {
    baseUrl: "https://www.haagendazs.us",
    specs: ["../specs/*.spec.js"],
      onPrepare() {
        browser.waitForAngularEnabled(false);
        // afterEach(() => browser.sleep(1000));
      },
        // highlightDelay: 1000
  };

  return Object.assign(
    {},
    defaultConfig,
    providedConfig
  );
};