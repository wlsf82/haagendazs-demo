class Homepage {
  constructor() {
    this.searchIcon = element(by.id("searchDisplay"));
    this.searchInputField = element(by.className("form-search"));
  }
}

module.exports = Homepage;